package Testcases;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import Locators.WebLocators;
import Pages.ConfluenceDashboard;
import Pages.Login;
import Pages.SpaceOnPageList;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import static DriverManager.WebDriverSingleton.prop;

public class SetRestrictionPage extends BaseClass implements WebLocators {

    @Test
    public void SetRestriction() throws InterruptedException {
        elog=extent.createTest("Set Restriction on Page","This test case will allow you to login to system and go and change the restriction of page");
        elog.log(Status.INFO,"Browser succesfully opened");
        driver.get(prop.getProperty("URL_SIGNIN"));
        elog.log(Status.INFO,"Wait for URL to Load");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        elog.log(Status.INFO, MarkupHelper.createLabel("Login Page open", ExtentColor.GREY));
        Login login= PageFactory.initElements(driver,Login.class);
        login.loginPage();
        elog.log(Status.INFO,"Login successful");
        elog.log(Status.INFO, MarkupHelper.createLabel("Performing operation on created Space", ExtentColor.GREY));
        ConfluenceDashboard confluenceDashboard=PageFactory.initElements(driver,ConfluenceDashboard.class);
        elog.log(Status.INFO,"Click on Space created");
        confluenceDashboard.spaceForPage();
        elog.log(Status.INFO, MarkupHelper.createLabel("Open Page menu select existing page and set restrction", ExtentColor.GREY));
        confluenceDashboard.PageInSpaceMenuOption();
        SpaceOnPageList spaceOnPageList=PageFactory.initElements(driver,SpaceOnPageList.class);
        spaceOnPageList.selectPage();
        elog.log(Status.INFO,"Selecting page on which restriction to be set");
        spaceOnPageList.clickonRestrictionPageMenu();
        elog.log(Status.INFO,"Page restriction set succesfully");
        AssertJUnit.assertTrue(true);
        elog.log(Status.PASS,"Restriction to exising page set succesfully");
    }
}
