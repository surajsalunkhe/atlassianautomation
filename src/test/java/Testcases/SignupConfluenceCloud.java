package Testcases;

import CommonUtils.FunctionFactory;
import DriverManager.WebDriverSingleton;
import Locators.WebLocators;
import Pages.ChooseProduct;
import Pages.SignUp;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static DriverManager.WebDriverSingleton.prop;

public class SignupConfluenceCloud extends BaseClass implements WebLocators{

    @Test
    public void SignupCloud()
    {
        String title="Try Hosted Software Development Tools | Atlassian Cloud";
        elog=extent.createTest("Sign Up to Atlassian cloud","This test case will allow you to sign up for atlassian cloud");
        driver.get(prop.getProperty("URL_SIGNUP"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        elog.log(Status.INFO,"Opened browser with URL");
        ChooseProduct chooseProduct= PageFactory.initElements(driver,ChooseProduct.class);
        elog.log(Status.INFO, MarkupHelper.createLabel("Choose Package", ExtentColor.BLUE));
        chooseProduct.SelectPlan(xpath_choosePacakage2);
        SignUp signUp=PageFactory.initElements(driver,SignUp.class);
        elog.log(Status.INFO, MarkupHelper.createLabel("Sign UP", ExtentColor.BROWN));
        signUp.SignupPage();
        Assert.assertTrue(true);
        elog.log(Status.PASS, MarkupHelper.createLabel("Succesfully Registered", ExtentColor.GREEN));

    }
}
