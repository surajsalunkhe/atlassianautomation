package Testcases;

import DriverManager.WebDriverSingleton;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import static CommonUtils.ScreenShotUtil.capscreen;

public class BaseClass {
    WebDriver driver;
    ExtentHtmlReporter htmlReporter;
    ExtentReports extent;
    ExtentTest elog;
    static Logger logger = Logger.getLogger(BaseClass.class);
    @BeforeSuite
    public void BaseSetup(){
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir")+"/Reports/MyOwnReport.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo("OS", "Mac OS Sierra");
        extent.setSystemInfo("Host Name", "Suraj");
        extent.setSystemInfo("Environment", "QA");
        extent.setSystemInfo("User Name", "Suraj Salunkhe");
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("AutomationTesting.in Demo Report");
        htmlReporter.config().setReportName("Test Report");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.STANDARD);
    }
    @BeforeMethod
    public void TestSetup()
    {
        logger.info("In before method");
        driver= WebDriverSingleton.getWebDriverInstance();
        driver.manage().window().maximize();
    }
    @AfterTest
    public void TearDownTest()
    {
        logger.info("In after test method");
        driver.quit();
        logger.info("In after test method quiting driver");
    }
    @AfterMethod
    public void getResult(ITestResult result){
        if(result.getStatus() == ITestResult.FAILURE)
        {
            elog.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
            capscreen(driver,result.getName());
            elog.fail(result.getThrowable());
        }
        else if(result.getStatus() == ITestResult.SUCCESS)
        {
            elog.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
        }
        else
        {
            elog.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
            elog.skip(result.getThrowable());
        }
        logger.info("In after method");
        driver.quit();
        logger.info("In after method driver quited succesfully");
    }
    @AfterSuite
    public void tearDownSuite()
    {
        logger.info("In After Suite method");
        extent.flush();
        logger.info("Flushed extent report");

    }
}
