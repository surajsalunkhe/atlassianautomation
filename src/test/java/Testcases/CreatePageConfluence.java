package Testcases;

import Locators.WebLocators;
import Pages.ConfluenceDashboard;
import Pages.Login;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static CommonUtils.ScreenShotUtil.capscreen;
import static DriverManager.WebDriverSingleton.prop;

public class CreatePageConfluence extends BaseClass implements WebLocators {

    @Test
    public void CreatePage() throws InterruptedException {
        elog=extent.createTest("Confluence-Create page using automation","This test case will allow you to login to system and go to created space and create blank page in atlassian cloud");
        elog.log(Status.INFO,"Browser succesfully opened");
        driver.get(prop.getProperty("URL_SIGNIN"));
        elog.log(Status.INFO,"Wait for URL to Load");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        elog.log(Status.INFO, MarkupHelper.createLabel("Login Page open", ExtentColor.GREY));
        Login login= PageFactory.initElements(driver,Login.class);
        login.loginPage();
        elog.log(Status.INFO,"Login Successful");
        ConfluenceDashboard confluenceDashboard=PageFactory.initElements(driver,ConfluenceDashboard.class);
        elog.log(Status.INFO, MarkupHelper.createLabel("Go to created space", ExtentColor.GREY));
        confluenceDashboard.spaceForPage();
        elog.log(Status.INFO,"Page Menu in created space");
        confluenceDashboard.PageInSpaceMenuOption();
        elog.log(Status.INFO, MarkupHelper.createLabel("Create page and publish page", ExtentColor.GREY));
        confluenceDashboard.CreateAndPublishBlankPage();
        Assert.assertTrue(true);
        elog.log(Status.PASS,"Page created and published successfully");

    }
}
