package CommonUtils;

import com.sun.jmx.snmp.Timestamp;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.util.Date;

public class ScreenShotUtil {
    public static WebDriver capscreen(WebDriver driver)
    {
        try {
            TakesScreenshot ts=(TakesScreenshot) driver;
            File src=ts.getScreenshotAs(OutputType.FILE);
            Date d=new Date();
            Timestamp t = new Timestamp(d.getTime());
            String timeStamp = t.toString();
            timeStamp = timeStamp.replace(' ', '_');
            timeStamp = timeStamp.replace(':', '_');
            String dest=System.getProperty("user.dir")+"/Screenshots/"+timeStamp+".png";
            File destination=new File(dest);
            FileUtils.copyDirectory(src, destination);
            System.out.println("Screenshot captured");
        } catch (Exception e) {
            System.out.println("Exception while taking screenshot"+e.getMessage());
        }
        return driver;

    }//end of capscreen
    public static WebDriver capscreen(WebDriver driver,String screenshotname) {
        try {
            TakesScreenshot ts = (TakesScreenshot) driver;
            File src = ts.getScreenshotAs(OutputType.FILE);
            Date d = new Date();
            Timestamp t = new Timestamp(d.getTime());
            String timeStamp = t.toString();
            timeStamp = timeStamp.replace(' ', '_');
            timeStamp = timeStamp.replace(':', '_');
            String dest = System.getProperty("user.dir") + "/Screenshots/" + screenshotname + timeStamp + ".png";
            File destination = new File(dest);
            FileUtils.copyDirectory(src, destination);
            System.out.println("Screenshot captured");
        } catch (Exception e) {
            System.out.println("Exception while taking screenshot" + e.getMessage());
        }
        return driver;
    }

}
