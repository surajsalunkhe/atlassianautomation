package CommonUtils;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class FunctionFactory {
    private static WebDriver driver;
    private static Logger logger=Logger.getLogger(FunctionFactory.class);

    public FunctionFactory(WebDriver driver){
        logger.info("In function factory driver");
        this.driver=driver;
    }
    public static String getTitle(){
        String title=driver.getTitle();
        System.out.println("In function factory constructor"+title);
        return title;
    }
    public static WebDriver LoadURL(String url)
    {
        try {
            driver.get(url);
            logger.info("URL added");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("Unable to navigate"+e.getMessage());
        }
        return driver;
    }
    public static WebDriver switchToWindow(WebDriver driver) throws InterruptedException {
        Set<String> windows = driver.getWindowHandles();
        Iterator<String> itr = windows.iterator();
        String windowID = null;
        logger.info("Getting winodw handles");
        while(itr.hasNext()){
            windowID = itr.next();
        }
        logger.info("New window ID="+windowID);
        driver.switchTo().window(windowID);
        Thread.sleep(3000);
        return driver;
    }
    public static WebDriver dragDrop(WebDriver driver,WebElement ele1, WebElement ele2 ) {
        try {
            WebElement From=ele1;
            WebElement To=ele2;
            Actions builder = new Actions(driver);
            Action dragAndDrop = builder.clickAndHold(From).moveToElement(To).release(To).build();
            dragAndDrop.perform();
            logger.info("Drag and drop action performed");

        }catch(Exception e) {
            logger.info("Unable to perform Drag and drop action");
        }
        return driver;
    }
    public static WebDriver holdandclick(WebDriver driver,WebElement element1,WebElement element2){
        Actions act=new Actions(driver);
        //act.clickAndHold(element1).
        act.moveToElement(element1).build().perform();
        element2.click();
        logger.info("Item selected from list menu");
        return driver;
    }
    public static WebDriver hoverclick(WebDriver driver,WebElement element1, WebElement element2 ) {
        try {
            WebElement ele1=element1;
            if(ele1.isDisplayed())
            {
                WebElement ele2=element2;
                if(ele2.isEnabled())
                {
                    Actions act = new Actions(driver);
                    act.moveToElement(ele1).build().perform();
                    act.click(ele2);
                    logger.info("sucesfully clicked");
                }else{
                    ele1.click();
                    logger.info("No element is enabled found for click");
                }

            }else{
                logger.info("clickable element is not exist");
            }

        }catch(Exception e) {
            logger.info("exception occured while hover click"+e.getMessage());
        }
        return driver;
    }
    public static WebDriver usingActionClick(WebDriver driver, WebElement element){
        Actions act=new Actions(driver);
        act.moveToElement(element).click().perform();
        return driver;
    }
    public static WebDriver waitForElement(WebDriver driver,WebElement element)
    {
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return driver;
    }
    public static WebDriver waitForLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(pageLoadCondition);
        return driver;
    }

    public static String simpleAlert()
    {
        Alert alt=driver.switchTo().alert();
        String alt_text=alt.getText();
        alt.accept();
        return alt_text;
    }
    public static String confirmAlert(String stat)
    {
        Alert alt=driver.switchTo().alert();
        String alt_text=alt.getText();
        if(stat.equalsIgnoreCase("Accept")){
            alt.accept();
        }else{
            alt.dismiss();
        }
        return alt_text;
    }
    public static String promptAlert(String stat,String data)
    {
        Alert alt=driver.switchTo().alert();
        String alt_text=alt.getText();
        alt.sendKeys(data);
        alt.accept();
        return alt_text;
    }
    public static WebDriver scriptTimeout()
    {
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        return driver;
    }
    public static WebDriver pageLoadTimeout()
    {
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        return driver;
    }
    public static WebElement searchElement(String locatorkey){
        WebElement element=null;
        if((element=driver.findElement(By.id(locatorkey)))==null) {
            logger.info("WebElement found by ID");
            return element;
        }else if((element=driver.findElement(By.name(locatorkey)))!=null){
            logger.info("WebElement found by Name");
            return element;
        }else if((element=driver.findElement(By.linkText(locatorkey)))!=null){
            logger.info("WebElement found by LinkText");
            return element;
        }else if((element=driver.findElement(By.partialLinkText(locatorkey)))!=null){
            logger.info("WebElement found by PartialLinkText");
            return element;
        }else if((element=driver.findElement(By.className(locatorkey)))!=null){
            logger.info("WebElement found by ClassName");
            return element;
        }else if((element=driver.findElement(By.cssSelector(locatorkey)))!=null){
            logger.info("WebElement found by CSS Selector");
            return element;
        }else
            element=driver.findElement(By.xpath(locatorkey));
            logger.info("WebElement found by XPATH Selector");
            return element;
    }
}//end of Function Factory
