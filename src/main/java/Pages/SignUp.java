package Pages;

import Locators.WebLocators;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static CommonUtils.FunctionFactory.waitForElement;
import static DriverManager.WebDriverSingleton.prop;


public class SignUp implements WebLocators {
    WebDriver driver;
    Logger logger = Logger.getLogger(SignUp.class);
    public SignUp(WebDriver ldriver)
    {
        this.driver=ldriver;
    }

    @FindBy(id = id_firstname)
    private WebElement firstName;
    @FindBy(id = id_lastname)
    private WebElement lastName;
    @FindBy(id = id_email)
    private WebElement email;
    @FindBy(id = id_accountname)
    private WebElement accountName;
    @FindBy(id=id_password)
    private WebElement password;
    @FindBy(className = class_submit)
    private WebElement submitButton;
    @FindBy (xpath = "//*[text()=\"Great, check your inbox\"]")
    private String textpresent;

    public WebDriver SignupPage()
    {
        accountName.clear();
        accountName.sendKeys(prop.getProperty("AccountName"));
        firstName.clear();
        firstName.sendKeys(prop.getProperty("FirstName"));
        lastName.clear();
        lastName.sendKeys(prop.getProperty("LastName"));
        email.clear();
        email.sendKeys(prop.getProperty("Email"));
        password.clear();
        password.sendKeys(prop.getProperty("Password"));
        if(submitButton.isEnabled()){
         submitButton.submit();
        }else{
            waitForElement(driver,submitButton);
            submitButton.submit();
        }
        return driver;
    }
}
