package Pages;

import Locators.WebLocators;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static DriverManager.WebDriverSingleton.prop;

public class Login implements WebLocators {
    WebDriver driver;
    Logger logger = Logger.getLogger(SignUp.class);
    public Login(WebDriver ldriver)
    {
        this.driver=ldriver;
    }
    @FindBy(id = id_username)
    private WebElement username;
    @FindBy(xpath =xpath_logincontinue)
    private WebElement logincontinue;
    @FindBy(id= id_loginpassword)
    private WebElement passwordlogin;
    @FindBy(xpath = xpath_loginsubmit)
    private WebElement log_in;

    public ConfluenceDashboard loginPage() throws InterruptedException {
        logger.info("In Login Page");
        logger.info("Clearing the saved or cahced username");
        username.clear();
        logger.info("Entering Username");
        username.sendKeys(prop.getProperty("Username"));
        logger.info("Clicked on login continue");
        logincontinue.click();
        Thread.sleep(5000);
        passwordlogin.clear();
        logger.info("Enter the password");
        passwordlogin.sendKeys(prop.getProperty("Password"));
        Thread.sleep(3000);
        logger.info("login button submit");
        log_in.click();
        return new ConfluenceDashboard(driver);
    }
}
