package Pages;

import CommonUtils.FunctionFactory;
import DriverManager.WebDriverSingleton;
import Locators.WebLocators;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static CommonUtils.FunctionFactory.searchElement;

public class ChooseProduct {
    WebDriver driver;
    Logger logger = Logger.getLogger(ChooseProduct.class);
    public ChooseProduct(WebDriver driver){
        logger.info("In choose product diver instance");
        this.driver=driver;
    }
    public SignUp SelectPlan(String locator){
        logger.info("In select plan");
        driver.findElement(By.xpath(locator)).click();
        return new SignUp(driver);
    }
}
