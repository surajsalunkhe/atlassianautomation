package Pages;

import Locators.WebLocators;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static CommonUtils.FunctionFactory.*;

public class SpaceOnPageList implements WebLocators {
    WebDriver driver;
    Logger logger = Logger.getLogger(SpaceOnPageList.class);
    public SpaceOnPageList(WebDriver driver)
    {
        this.driver=driver;
    }
    @FindBy(xpath = xpath_pagefromlist)
    private WebElement pagefromlist;
    @FindBy(id = id_actionmenu)
    private WebElement actionmenu;
    @FindBy(id = id_actionmenu_permission_link)
    private WebElement permsissionlink;
    @FindBy(xpath = xpath_editing_restricted)
    private WebElement editingrestricted;
    @FindBy(id = id_apply_restriction)
    private WebElement apply;
    @FindBy(className = class_restrictiondropdown)
    private WebElement restrictdropdown;
    @FindBy(xpath = xpath_norestriction)
    private WebElement norestriction;
    @FindBy(xpath = xpath_view_edit_restricted)
    private WebElement view_edit_restricted;

    public WebDriver selectPage()
    {
        logger.info("select page from list");
        waitForElement(driver,pagefromlist);
        pagefromlist.click();
        logger.info("Page selected");
        return driver;
    }
    public WebDriver clickonRestrictionPageMenu() throws InterruptedException {
        waitForLoad(driver);
        waitForElement(driver,actionmenu);
        logger.info("clicking on restriction link");
        actionmenu.click();
        logger.info("succesfully clicked on restriction link");
        waitForElement(driver,permsissionlink);
        permsissionlink.click();
        logger.info("On permission link page");
        Thread.sleep(10000);
        switchToWindow(driver);
        logger.info("click on drop down to set restriction");
        waitForElement(driver,restrictdropdown);
        restrictdropdown.click();
        waitForElement(driver,editingrestricted);
        usingActionClick(driver,editingrestricted);
        Thread.sleep(4000);
        restrictdropdown.click();
        logger.info("again click on dropdown");
        usingActionClick(driver,norestriction);
        logger.info("selected no resriction");
        Thread.sleep(3000);
        logger.info("Click again on restriction dropdown");
        restrictdropdown.click();
        waitForElement(driver,editingrestricted);
        usingActionClick(driver,editingrestricted);
        logger.info("Clicked on editing retsricted");
        apply.click();
        logger.info("Clicked on apply button");
        driver.switchTo().defaultContent();
        logger.info("Switched to default window");
        logger.info("Restriction set succesfully");
        return driver;
    }
}
