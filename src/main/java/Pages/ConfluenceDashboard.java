package Pages;

import Locators.WebLocators;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Random;

import static CommonUtils.FunctionFactory.*;

public class ConfluenceDashboard implements WebLocators {
    WebDriver driver;
    Logger logger = Logger.getLogger(ConfluenceDashboard.class);
    public ConfluenceDashboard(WebDriver driver)
    {
        this.driver=driver;
    }
    @FindBy(xpath = xpath_space_technosnoop)
    private WebElement space;
    @FindBy(xpath = xpath_space_pagemenu)
    private WebElement pagemenu;
    @FindBy(xpath = xpath_createpage)
    private WebElement createPage;
    @FindBy(xpath = xpath_blankPage)
    private  WebElement blankPagecreate;
    @FindBy(xpath = xpath_createPageButton)
    private WebElement createPageButton;
    @FindBy(id =id_publishButton)
    private WebElement publishButton;
    @FindBy(xpath = xpath_textType)
    private WebElement typeText;
    Random rand=new Random();
    int pagenumber = rand.nextInt(900000000) + 100000000;

    public WebDriver spaceForPage() throws InterruptedException {
        waitForLoad(driver);
        logger.info("In Dashboard");
        waitForElement(driver,space);
        try {
            space.click();
        }
        catch (org.openqa.selenium.StaleElementReferenceException ex){
            space.click();
        }
        logger.info("In Spaces menu");
        return driver;
    }

    public WebDriver PageInSpaceMenuOption() throws InterruptedException {
        waitForLoad(driver);//wait till page get loaded
        waitForElement(driver,pagemenu);//wait till page menu displayed
        pagemenu.click();
        logger.info("Clicked on Page menu");
        return driver;
    }

    public WebDriver CreateAndPublishBlankPage() throws InterruptedException {
        waitForElement(driver,createPage);//Wait till create page button not found
        createPage.click();
        Thread.sleep(8000);
        String BaseWindowHandleId=driver.getWindowHandle();
        logger.info("My current window Handle ID"+BaseWindowHandleId);
        switchToWindow(driver);
        logger.info("Window switched succesfully");
        blankPagecreate.click();
        logger.info("Clicked on blank page create button succesfully");
        createPageButton.click();
        logger.info("Clicked on Create Page button succesfully");
        waitForLoad(driver);
        driver.switchTo().defaultContent();
        waitForElement(driver,typeText);
        Thread.sleep(5000);
        typeText.sendKeys("Automated Page created="+pagenumber);
        Thread.sleep(3000);
        publishButton.click();
        logger.info("Automated page published succesfully");
        return driver;
    }
}
