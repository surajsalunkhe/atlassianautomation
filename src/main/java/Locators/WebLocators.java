package Locators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public interface WebLocators {

    //#######Choose Package Page Locators########
    public final String xpath_choosePacakage1="//img[@alt='Confluence only']";
    public final String xpath_choosePacakage2="//img[@alt='Confluence for software teams']";
    public final String xpath_choosePacakage3="//img[@alt='Confluence for IT teams']";

    //#######Signup Page Locator########
    String id_firstname="firstName";
    String id_lastname="lastName";
    String id_accountname="accountName";
    String id_email="email";
    String id_password="aod-password";
    String class_submit="wac-button cta";

    //##### Log In to Confluence #####
    String id_username="username";
    String xpath_logincontinue="//*[text()=\"Continue\"]";
    String id_loginpassword="password";
    String xpath_loginsubmit="//*[text()=\"Log in\"]";

    //### Spaces for create page
    String xpath_space_technosnoop="//span[text()=\"Technosnoop\"]";
    String xpath_space_pagemenu="//a[@href='/wiki/spaces/TEC/pages']";
    String xpath_createpage="//*[text()=\"Create page\"]";
    String xpath_blankPage="//*[text()=\"Blank page\"]";
    String xpath_createPageButton="//button[text()=\"Create\"]";
    String id_publishButton="rte-button-publish";
    String xpath_textType="//input[@id=\"content-title\"]";

    //### List of Pages####
    String xpath_pagefromlist="//*[@class=\"link-title\"][text()=\"Test Page content\"]";
    String id_actionmenu="action-menu-link";
    String id_actionmenu_permission_link="action-page-permissions-link";
    String class_restrictiondropdown="select2-choice";
    String xpath_editing_restricted="//*[text()=\"Everyone can view, only some can edit\"]";
    String id_apply_restriction="page-restrictions-dialog-save-button";
    String xpath_norestriction="//*[text()=\"Everyone can view and edit\"]";
    String xpath_view_edit_restricted="//*[text()=\"Viewing and editing restricted\"]";
}
