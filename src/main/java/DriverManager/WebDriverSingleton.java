/**
 * 
 */
package DriverManager;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Suraj
 *
 */
public class WebDriverSingleton {
    private static WebDriver driver;
    public static String Browser;
    public static Properties prop = new Properties();
    static InputStream input = null;
    static Logger logger = Logger.getLogger(WebDriverSingleton.class);

    private WebDriverSingleton(){
    }
    public static WebDriver getWebDriverInstance(){
        logger.info("In WebDriver Instance");
        try {
            input = new FileInputStream("Property/path.properties");
            prop.load(input);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Browser=System.getProperty("browser");
        if(Browser==null )
        {
            Browser="Chrome";
        }
        if(Browser.equalsIgnoreCase("Firefox"))
            {
                logger.info("Setting Firefox driver for automation");
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+prop.getProperty("FireFoxPath"));
                driver=new FirefoxDriver();
                logger.info("Firefox driver Instance Set succesfully");

            }if(Browser.equalsIgnoreCase("Safari")){
                logger.info("Setting Safari driver for automation");
                driver=new SafariDriver();
                logger.info("Safari driver set succesfully");
            }
            if(Browser.equalsIgnoreCase("Chrome")) {
                logger.info("Setting Chrome driver for automation");
                System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ prop.getProperty("CHROME_PATH"));
                driver = new ChromeDriver();
                logger.info("Chrome driver set succesfully");
            }

        logger.info("returning driver control to calling method");
        return driver;
    }
}
