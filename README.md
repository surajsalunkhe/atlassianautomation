# AtlassianAutomation

Test automation framework for Spartez (automating confluence)
1.Framework designed using page factory and Testng.

2. Framework is designed in keeping mind that environment variable set for java .
Successfully work on Mac OS and Linux OS.(As Driver present in the repository are for mac os)
Tested on Chrome browser.
Run command:

Run individual test cases using TestNG located under "src/test/java/Testcases/"
i. CreatePageConfluence
ii. SetRestrictionPage

3. To run this framework pass maven goal as with different browser ( This step is in Progress due to time constraint and error resolution )
	mvn install -Dbrowser=Firefox/Chrome/Safari

or you can run as testng.xml using TestNG runner (default browser is set as Chrome if no parameter passed). Currently 3 major browsers are supported.

4. Extent report is used for reporting after execution report can be viewed in Report Folder.

5. Locators are stored in Locator folder in WebLocator File.

6. UserDetails for login and can be updated in Property> path.properties file

7. Individual Tests can be runned as by right clicking and Run as TestNG test.
These test cases located in "src/test/java/Testcases/"
i. CreatePageConfluence
ii. SetRestrictionPage

8. Logs will be generated log folder using log4j file. 3 types of log file generated for user readbale format.

Assumptions or prerequisite before executing :
User is successfully registered for confluence and able to login using.
Space should be created for Pages. Currently for surajsalunkhe.atlassian.net/wiki , created space as Technosnoop and utilised further for automation. (To run for other user create space with same name or update the locator path to your space.
To automate restriction on already present page scenario,  one Test page is already created with name “Test Page Content” and utilised for setting restriction.
So both space and page should be present.(Updated according to your account details or scenario) in your account or script will get failed.


